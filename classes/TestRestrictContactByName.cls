@istest
public class TestRestrictContactByName {
    
    @isTest static void Testrestrictcontactbylastname() {
        
        contact con = new contact(LastName='INVALIDNAME');
        insert con;
        
        // Perform test
        Test.startTest();
        Database.saveResult result = Database.insert(con, false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "'+con.LastName+'" is not allowed for DML',
                             result.getErrors()[0].getMessage());
    }
}