@isTest
public class TestVerifyDate {
    
    @isTest static void withinthirty() {
        date startdate=date.newInstance(2019, 10, 19);
        date enddate=date.newInstance(2019, 10, 29);
        Date vydate = VerifyDate.CheckDates(startdate , enddate);
        System.assertEquals(enddate,vydate);
    }
	@isTest static void notwithinthirty() {
        date startdate=date.newInstance(2019, 10, 19);
        date enddate=date.newInstance(2019, 11, 29);
        Date vydate1 = VerifyDate.CheckDates(startdate , enddate);
        System.assertEquals(date.newInstance(2019, 10, 31),vydate1);
    }
    @isTest static void nextthirty() {
        date startdate=date.newInstance(2019, 10, 19);
        date enddate=date.newInstance(2019, 10, 12);
        boolean vydate2 = VerifyDate.DateWithin30Days(startdate , enddate);
        System.assertEquals(vydate2,false);
    }
    @isTest static void notnextthirty() {
        date startdate=date.newInstance(2019, 10, 19);
        date enddate=date.newInstance(2019, 10, 29);
        boolean vydate3 = VerifyDate.DateWithin30Days(startdate , enddate);
        System.assertEquals(vydate3,true);
    }
    @isTest static void setendmonth() {
        date startdate=date.newInstance(2019, 10, 19);
        date enddate=date.newInstance(2019, 10, 29);
        date vydate4 = VerifyDate.SetEndOfMonthDate(startdate);
        System.assertEquals(vydate4,date.newInstance(2019, 10, 31));
    }
    

}