global class ContactValidation implements ValidationInterface{
    
    global String  performValidation(Sobject mySobj){

        String enteredLastName = (String)mySobj.get(Schema.Contact.FirstName);
        if(String.isBlank(enteredLastName)){
            return 'First Name Cannot be Empty';
        }else{
           
            return 'success';
        }
            
    
}
}