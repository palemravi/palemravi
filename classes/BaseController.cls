public class BaseController {
  
 /* @AuraEnabled  
    public static String readMetaData(String Name)
{
    List<String> Fields=new List<String>(); 
    string a;
      Form__mdt[]  Fsets=[SELECT MasterLabel,QualifiedApiName,Configuration_Value__c FROM Form__mdt];
    for(Form__mdt Fset :Fsets){
       String Field=Fset.Configuration_Value__c;
        Fields.add(Field);
        if(Fset.QualifiedApiName== Name){  
             a=Fset.Configuration_Value__c;
                }
    			}
    return a;
} */
    //global override VisualEditor.DataRow readMetaData(){
 /* public static List<String> readMetaData()
{
    List<String> Fields=new List<String>();
     
      Form__mdt[]  Fsets=[SELECT MasterLabel,Configuration_Value__c FROM Form__mdt];
    for(Form__mdt Fset :Fsets){
       String Field=Fset.Configuration_Value__c;
        Fields.add(Field);
    }
    return Fields;
} 
*/
    
    
  public static List<String> readFieldSet(String Name, String ObjectName)
{

    //List<String> Fields=new List<String>(); 
    /*string c;
    string fieldSetName;
      Form__mdt[]  Fsets=[SELECT MasterLabel,QualifiedApiName,Configuration_Value__c FROM Form__mdt];
    for(Form__mdt Fset :Fsets){
       String Field=Fset.Configuration_Value__c;
        //Fields.add(Field);
        if(Fset.QualifiedApiName== Name){  
             c=Fset.Configuration_Value__c;
            break;
                }
    			}
    	fieldSetName=c;
*/
    // *************************************
    
    Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
    Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
    Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

    system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(Name));

    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(Name);

  	List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
    system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
   List<String> a=new List<String>();

				for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
						{
                            string b= fieldSetMemberObj.getFieldPath();
                        system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                        system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                        system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                        system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                        system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
                            a.add(b);
						} 
    return a;
}
}