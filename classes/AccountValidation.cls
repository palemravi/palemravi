global class AccountValidation implements ValidationInterface{
    //Use retuen type as ResponseWrapper object
    global String  performValidation(Sobject mySobj){

        String enteredName = (String)mySobj.get(Schema.Account.Name);
        String trimmedName=enteredName.trim();
        if(trimmedName.length()<5){
            return 'Account name should be greater than 5 characters';
            
        }else{
           
            return 'success';
            
        }
            //
    
}
}