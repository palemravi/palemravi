//Wrapper class for interating with the front end
public class ResponseWrapper {
    @AuraEnabled
    public String data;
    @AuraEnabled
    public String errorMessage;
    @AuraEnabled
    public Boolean status;
    
    public ResponseWrapper(String data, Boolean status, String errorMessage){
        this.data = data;
        this.status = status;
        this.errorMessage = errorMessage;
    }
    
    public ResponseWrapper(){}

}