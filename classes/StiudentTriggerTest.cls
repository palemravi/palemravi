@istest
public class StiudentTriggerTest {
    Static testMethod void MyTestFunction(){
        Student__c VarS=new Student__c();
        VarS.Age__c=25;
        VarS.Exp__c=9;
        
        Insert VarS;
        
        Student__c VarS2=new Student__c();
        VarS2=[Select Status__c From Student__c  Where Id= : VarS.Id];
        System.assertEquals('Selected', VarS2.Status__c);
    }

}