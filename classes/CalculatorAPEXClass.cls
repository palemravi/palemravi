public class CalculatorAPEXClass {

 //variable declaration at class level (not inside any function)
     private static integer VarSum; 
     private static integer VarMultiplication; 
 
 //function for sum
     public static integer MakeSum(integer VarNumA, integer VarNumB) { 
  // write "integer" than "void"

  VarSum = VarNumA + VarNumB;
  return VarSum; 
        
  // return is a keyword which returns the value written in front of it to caller
  //Sum is class level variable 
  //NumA,NumB is local / function level variable
    }
 
 //function for multiplication
  public static void MakeMultiply(integer VarNumC, integer VarNumD) {
 
  VarMultiplication = VarNumC * VarNumD;
  system.debug('The answer = ' + VarMultiplication  );
    }
 
}