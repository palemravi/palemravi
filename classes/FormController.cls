public class FormController {
    
    
    
    @AuraEnabled  
    public static list<string> readObjectName(String nameOfObject)//The variable names should be of camel case. So change it.
    {
        List<String> Fields=new List<String>(); 
        Form__mdt[]  Fsets1=[SELECT Configuration_Value__c FROM Form__mdt];
        for(Form__mdt Fset :Fsets1){
            String Field=Fset.Configuration_Value__c;
            Fields.add(Field);
        }
        system.debug(Fields[0]);
        String jsonInput = Fields[0];
        system.debug(jsonInput);
        Map<String, String> m = 
            (Map<String, string>)
            JSON.deserialize(jsonInput,Map<String, String>.class);//There is no need for using deserialiseUntyped. Use deserialised untyped if the
        system.debug(m);//data type of the values are not constant. In this case the data type of values will be always be a string
        string z = (string)m.get(nameOfObject);//Convert the String into Map<String,String> structure
        system.debug(z);
        list<string> returnValFromBaseController = BaseController.readFieldSet(z,nameOfObject);
        return returnValFromBaseController;
        //Use ResponseWrapper and a try catch statement;
        
    }
    
    //For your reference
    @AuraEnabled 
    public static ResponseWrapper getInitialData(String objectApiName){
        try{
            ResponseWrapper response = new responseWrapper();
            response.status = true;
            Form__mdt[]  fsetConfigList=[SELECT Configuration_Value__c FROM Form__mdt];
            //check if metadata record exists, else throw error
            if(fsetConfigList != null && fsetConfigList.size() > 0){
                //check if configuration field is empty is empty. If empty then throw error
                if(String.isNotBlank(fsetConfigList[0].Configuration_Value__c)){
                    //use this type of deserialization if you know the structure already
                    Map<String,Object> objectApiNameFieldSetMap =  (Map<String, Object>)JSON.deserializeUntyped(fsetConfigList[0].Configuration_Value__c);
                    Map<String, Object> dim =(Map<String, Object>)objectApiNameFieldSetMap.get(objectApiName);
						string z = (string)dim.get('fieldsetName'); 
                    
                    //Map<String, String> objectApiNameFieldSetMap = (Map<String, String>) JSON.deserialize(fsetConfigList[0].Configuration_Value__c, Map<String, String>.class);
                    //Check if given objectapiname has fieldset in the configuratione else throw error
                    if(objectApiNameFieldSetMap.containsKey(objectApiName)){
                        List<String> fieldSetApiNameList = BaseController.readFieldSet((string)dim.get('fieldsetName'),objectApiName);
                        response.data = JSON.serialize(fieldSetApiNameList);
                    } else {
                        throw new GenericException('No Field set configuration found for given object'+objectApiName);
                    }
                }else{
                    throw new GenericException(' Configuration field cannot be empty');
                }
            } else {
                throw new GenericException(' No Configuration record found');
            }
            return response;           
        }catch(Exception excp){
            return new ResponseWrapper('', false, excp.getMessage());           
        }
    }
    
    //Create a method called saveRecord
    //It should accept the paramater sObject
    //In the LDS get the form data,and form an Sobject of the given Object Api Name
    //Save the object in the class
    //Show toast as successfully saved , else show error toast with the error message
    //If saved succesfully show the created record's Id in the toast 

    @AuraEnabled  
    public static ResponseWrapper saveRecord(Sobject sobjectParam){
        //DML operation Save to Database
        
        // // if (a=='success'){
        //     insert sobjectParam;
        // return  sobjectParam.Id;
        // // }else{
        // //     return 'error';
        // // }
        
        try{
            ResponseWrapper response = new responseWrapper();
            response.status = true;
        insert sobjectParam;
        response.data=sobjectParam.Id;
        return response;  
        }catch(Exception excp){
            return new ResponseWrapper('', false, excp.getMessage());           
        }

    }
    @AuraEnabled
    public static ResponseWrapper  performValidation(Sobject sobjectParam){
        try{
            ResponseWrapper response = new responseWrapper();
            String objectApiName = string.valueof(sobjectParam.getSObjectType());
            Form__mdt[]  fsetConfigList=[SELECT Configuration_Value__c FROM Form__mdt];
             Map<String,Object> objectApiNameFieldSetMap =  (Map<String, Object>)JSON.deserializeUntyped(fsetConfigList[0].Configuration_Value__c);
             Map<String, Object> dim =(Map<String, Object>)objectApiNameFieldSetMap.get(objectApiName);
             string implementationClassName = (string)dim.get('validationClassName');   
            Type implementationClassInstance = Type.forName(implementationClassName);
            Type implementationInterface = ValidationInterface.class;
            Boolean isValidInterface = implementationInterface.isAssignableFrom(implementationClassInstance);
            if(!isValidInterface){
                throw new GenericException('Please input the class name which implements ValidationInterface interface');
            }
            ValidationInterface v = (ValidationInterface)implementationClassInstance.newInstance();
            string a=v.performValidation(sobjectParam);
            if(a=='success'){
                ResponseWrapper responseAfterSave = new responseWrapper();
                responseAfterSave= FormController.saveRecord(sobjectParam);
                if(responseAfterSave.status==true){
                    response.status=true;
                    response.data=responseAfterSave.data;
                    return response;
                }else if(!responseAfterSave.status){
                    return new ResponseWrapper('', false, 'Something went wrong');
                }
                return new ResponseWrapper('', false, 'Something went wrong');
            }else{
                response.status=false;
                response.data=a;
                 return response;   
            }
        }catch(Exception excp){
                return new ResponseWrapper('', false, excp.getMessage());           
            }   
} 
    //Create an interface called ValidationInterface
    //Create a method called performValidation which should accept the sobject as parameter
    //validation class as an attribute in the metadata json, and use that class name for performing validation   
}