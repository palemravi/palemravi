public class SaveRegistrationForm {

    @AuraEnabled
    public static Id saveRegDetails(Registration_Form__c regForm){
        //DML operation Save to Database
        
        insert regForm;
        return regForm.Id;
    }
    @AuraEnabled
    public static Id saveEduDetails(Educational_Detail__c EduDet){
        //DML operation Save to Database
        
        insert EduDet;
        return EduDet.Id;
    }
    
}