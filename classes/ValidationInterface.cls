global interface ValidationInterface{

      String  performValidation(Sobject mySobj);

}