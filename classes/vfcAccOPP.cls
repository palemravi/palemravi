public with sharing class vfcAccOPP {
    public list<Account> getAcc(){
        return [select Id,Name from Account ];
    }
    public list<Opportunity> getOpp(){
        return [select Id,Name from Opportunity ];
    }
}
