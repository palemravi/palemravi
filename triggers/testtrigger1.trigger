trigger testtrigger1 on Monster_com_Job_Form__c (after insert) {
    
    for(Monster_com_Job_Form__c a : Trigger.new) {
    Task aTask = new Task();
    aTask.ownerId = a.ownerId;
    aTask.subject = 'New Account Task';
    aTask.whatId = a.Id;
    aTask.priority = 'Normal';
    Insert aTask; 
  }

}