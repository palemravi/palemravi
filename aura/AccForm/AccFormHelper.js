({
    showToastMessage: function (component, type, message,flag) {
        var myMsg = component.get("v.msg");
        var toastEvent = $A.get("e.force:showToast");
        if(flag==true){
            toastEvent.setParams({
                "mode": "sticky",
                "title": type == 'success' ? 'SUCCESS' : 'ERROR',
                "message": message,
                "type": type,
                messageTemplate: '{1}!',
                messageTemplateData: [message, {
                    url: 'https://palemravi-dev-ed.lightning.force.com/' + myMsg,
                    label: message,
                }
                ]
            });
            toastEvent.fire();
        }else if(flag==false){
            toastEvent.setParams({
                "mode": "sticky",
                "title": type == 'success' ? 'SUCCESS' : 'ERROR',
                "message": message,
                "type": type,
        
            });
            toastEvent.fire();
        }
        
    }
})