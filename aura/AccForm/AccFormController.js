({
    // onSuccessAction: function (component, event, helper) {

    //     component.set("v.spinerFlag", false);

    //     console.log("Account has been created successfully.");
    //     // creates a new form
    //     var param = event.getParams();
    //     console.log(JSON.stringify(param));
    //     var recId = param.response.id; // this will return the record Id after success.
    //     console.log(recId);
    //     if (recId != '') {


    //         component.set("v.confirmationMsg", true);
    //         component.set("v.msg", recId);
    //         //alert(recId);
    //         //To Reload the web Page.
    //         //window.location.reload();
    //         helper.showToastMessage(component)
    //     }

    // },

    // onsubmitAction: function (component, event, helper) {
    //     component.set("v.spinerFlag", true);
    //     alert('Onsubmit Action')
    //     // event.preventDefault();// it just stops the standard submit.
    //     // var evtFields = event.getParam("fields"); // list of all the fields of that objectAppiName
    //     // evtFields["Rating"] = 'Hot';
    //     // evtFields["BillingCountry"] = 'India';
    //     // evtFields["BillingCity"] = 'Hyderabad';
    //     // component.find("rec").submit(evtFields);
    //     //alert('have successfully Submitted the Form');
    // },

    handleSave: function (component, event, helper) {
        component.set("v.spinerFlag", true);
        console.log('Saving record')
        var objectName = component.get("v.objName")
        var formDetails = component.find("myForm")
        console.log(formDetails);
        var sobject = { 'sobjectType': objectName }
        formDetails.forEach(element => {
            sobject[element.get('v.fieldName')] = element.get('v.value');
        });
        console.log(sobject)
        var action2 = component.get("c.performValidation")
        action2.setParams({
            'sobjectParam': sobject
        })
        action2.setCallback(this, function (response2) {
            var state2 = response2.getState();
            if (state2 === "SUCCESS") {
                var result2 = response2.getReturnValue()
                component.set("v.msg", result2.data);
                if (result2.status == true) {
                    helper.showToastMessage(component, 'success', 'The record has been successfully created ' + result2.data, result2.status);
                    component.set("v.spinerFlag", false);
                    component.set("v.reloadForm", false);
                    component.set("v.reloadForm", true);
                } else if (result2.status == false) {
                    
                    // var inputCmp = component.find("inputCmp");
                    // var value = inputCmp.get("v.value");
                    // // is input valid text?
                    // if (value === "John Doe") {
                    //     inputCmp.setCustomValidity("John Doe is already registered");
                    // }
                    helper.showToastMessage(component, 'error', ' ' + result2.data, result2.status);
                    component.set("v.spinerFlag", false);
                }
                // if (result2 == "success") {
                //     var action1 = component.get("c.saveRecord");
                //     action1.setParams({
                //         'sobjectParam': sobject,
                //     })
                //     action1.setCallback(this, function (response) {
                //         var state1 = response.getState();
                //         console.log(state1);
                //         if (state1 === "SUCCESS") {
                //             console.log(state1)
                //             var result = response.getReturnValue()
                //             console.log(result.data)
                //             component.set("v.msg", result.data);
                //             helper.showToastMessage(component, 'success', 'The record has been successfully created ' + result.data);
                //         }
                //         else if (state1 === "INCOMPLETE") {
                //             console.log('ITs Incomplete')
                //         }
                //         else if (state1 === "ERROR") {
                //             var errors = response.getError();
                //             if (errors) {
                //                 helper.showToastMessage(component, 'error', 'Something went wrong');
                //             }
                //         }
                //         component.set("v.spinerFlag", false);

                //     })
                // }
            } else if (state2 === "ERROR") {
                var errors = response2.getError();
                if (errors) {
                    helper.showToastMessage(component, 'error', 'Something went wrong', result2.status);
                }
            }
            component.set("v.spinerFlag", false);
        })
        $A.enqueueAction(action2)
        //$A.enqueueAction(action1)
        // $A.enqueueAction(action2)
    },
    onLoadAction: function (component, event, helper) {
        //alert('Component one Successfullyy'); 
        /*	var action1 = component.get("c.readMetaData");
             //alert('Component');
             action1.setParams({"Name":component.get("v.FieldSetName")});
         //console.log('This one is after Meta Data');
                 // setCallback 
         action1.setCallback(this,function(response){
             var state1 = response.getState();
             console.log(state1);
             if(state1 == 'SUCCESS'){
                 var result1 = response.getReturnValue();
                 console.log(result1);
                 component.set("v.ReturnedFieldSetName",result1);
               //alert(result1);   
                 component.set("v.MetadataReturnValues",result1);            
             }
             var a=result1;
         }); 
         var b=result1;
         */
        // console.log('This one is after loading');
        // Calling Server-side action 
        component.set("v.spinerFlag", true);
        var a = component.get("v.objName")
        component.set("v.myForm", a)
        var action = component.get("c.getInitialData");
        action.setParams({
            //"Name": component.get("v.FieldSetName")
            "objectApiName": a
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            //console.log(state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log(result);
                //alert(result); 
                var ser = JSON.parse(result.data);
                component.set("v.returnValues", ser);
                console.log('returned values are ', ser);
            }
            component.set("v.spinerFlag", false);
        });
        $A.enqueueAction(action);
        // $A.enqueueAction(action1);
    },
    // handleError: function (component, event, helper) {
    // }
})