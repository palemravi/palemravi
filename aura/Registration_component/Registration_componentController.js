({
	doSubmit : function(cmp, event, helper) {
        
        console.log('this came after clicking submit');
        //prompt('this is from submigt');
        //call apex func to save data in reg form also we have to pass parameters
		var regformdetails=cmp.get("v.Regform");
        
        //Create a one time use instance of the "saveRegDetails" action in the server side controller
        var action = cmp.get("c.saveRegDetails");
        action.setParams({"regForm" : regformdetails});
        //create a callback that is excecuted after the server-side returns
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state == "SUCCESS"){
                
                cmp.set("v.isDataSubmitted","True");
                				//alert the user with value returned
                				//from server
                				alert("ID is " +response.getReturnValue());  
            						
            var ParId=response.getReturnValue();
            cmp.set("v.RegistrationRecordId",ParId);
            }
            else if(state == "ERROR"){
                var errors= response.getErrors();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("erroe message: " +errors[0].message);
                    }
                }else{
                    console.log("unknown error");
                }
                
            }
            
        });
        $A.enqueueAction(action);
},
		onchangecheckbox:function(component, event, helper) {
        console.log('this came after clicking checkbox')
        helper.onclickhelper(component, event);
    },
    EducationaldetailsButton : function(component, event, helper) {
       var currenteducationaldetaillist=component.get("v.EducationalDetails");
        var newsize= parseInt((currenteducationaldetaillist.length)+1);
        currenteducationaldetaillist.push(newsize);
        component.set("v.EducationalDetails",currenteducationaldetaillist);
        
    }
})