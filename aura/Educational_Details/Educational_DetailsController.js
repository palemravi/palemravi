({
	deleteEducationaldetails : function(component, event, helper) {
		var NewEducationdalDetailList = component.get("v.EducationalDetailsInnerComponent");
        var currentIndex = component.get("v.indexNo");
        if(currentIndex > -1)
            NewEducationdalDetailList.splice(currentIndex,1);
        component.set("v.EducationalDetailsInnerComponent",NewEducationdalDetailList);
        
	},
    doinit : function(component, event, helper) {
        console.log("This is from do init");
        helper.helperMethod1(component,event);
    },
    saveEducationalDetails:function(component, event) {
        console.log("This is from do chage function");
        var regid=component.get("v.RegRecordId");
        component.set("v.Edu.Registration_Form__c",regid);
        var EduDetails=component.get("v.Edu");
        var actn=component.get("c.saveEduDetails");
        actn.setParams({EduDet : EduDetails });
        actn.setCallback(this,function(resp){
            var state = resp.getState();
            if(state == "SUCCESS"){
                console.log( 'Add Educational Detail is saved');
                
            }
            else if(state === "ERROR"){
                var errors = resp.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
       
                
            }
            
        });
        
        $A.enqueueAction(actn);
        
    }
})