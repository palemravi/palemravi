({
    doInit: function (cmp) {
        let initialData=cmp.get("v.options")
        console.log("Initial dAta is",initialData)
        initialData.forEach(function(element,index){
            element.selected =true
        })
        cmp.set("v.options",initialData)
        console.log("Transformed dAta is",initialData)
        // alert("Con")
    },
    showOptions:function(cmp){
 console.log("Showing Fields")
        var click=cmp.get("v.showValues")
        if(!click){
            $A.util.addClass(cmp.find('resultsDiv'),'slds-is-open');  
            cmp.set("v.showValues",true)
        }else{
            $A.util.removeClass(cmp.find('resultsDiv'),'slds-is-open');
            cmp.set("v.showValues",false)
        }
        // var disabled = cmp.get("v.disabled");
        // if(!disabled) {
        //     var options = cmp.get("v.options");
        //     options.forEach( function(element,index) {
        //         element.isVisible = true;
        //     });
        //     console.log(options)
        //     cmp.set("v.options", options); 
        // }
        

    },
    mouseout:function(cmp,event){
        if(!cmp.get('v.blur')){
            $A.util.removeClass(cmp.find('resultsDiv'),'slds-is-open');
            cmp.set("v.showValues",false)
            cmp.set("v.blur",true) 
        } else{
            cmp.set("v.blur",false) 
        }
    },
    selectItem:function(cmp,event){
        $A.util.addClass(cmp.find('resultsDiv'),'slds-is-open'); 
        var options = cmp.get('v.options');
        var values = cmp.get('v.values') || [];
        // var count = 0;
        options.forEach( function(element, index) {
            if(element.value === event.currentTarget.id) {
                    if(values.includes(element.value)) {
                        values.splice(values.indexOf(element.value), 1);
                        cmp.set("v.selected",true)
                       
                    } else {
                        values.push(element.value);
                        cmp.set("v.selected",false)                            
                    }
                    element.selected = element.selected ? false : true; 
                    cmp.set("v.blur",true)   
            }
            $A.util.addClass(cmp.find('resultsDiv'),'slds-is-open');
            // if(element.selected) {
            //     count++;
            // }
        });
        
        console.log(values.join(";"))
        cmp.set('v.options', options);
        // cmp.set("v.value","value selected is "+event.currentTarget.id)
        // cmp.set("v.value", values.join(";"));
        cmp.set("v.value", values.length + ' options selected');
        // $A.util.removeClass(cmp.find('resultsDiv'),'slds-is-open');
        console.log("Frary  ")
    },
    onblur:function(cmp,event){
        console.log("onblur activated")
        if(!cmp.get('v.blur')){
            $A.util.removeClass(cmp.find('resultsDiv'),'slds-is-open');
            cmp.set("v.showValues",false)
            cmp.set("v.blur",true) 
        } else{
            cmp.set("v.blur",false) 
        }
        // }if(event.currentTarget.id){
        //     // $A.util.removeClass(cmp.find('resultsDiv'),'slds-is-open');
        //     cmp.set("v.showValues",true)
        //     console.log(event.currentTarget.id)
        //     console.log("There is Target id")
        //     cmp.set("v.blur",false)  
        // }
           
    },
    handleChange: function (cmp, event) {
        $A.util.addClass(cmp.find('resultsDiv'),'slds-is-open');
        // cmp.set("v.selected",true)
        // // This will contain the string of the "value" attribute of the selected option
        // var selectedOptionValue = event.getParam("myValue");
        // alert("Option selected with value: '" + selectedOptionValue + "'");
        // console.log("Saving in progress")
        // // cmp.set("v.myValue","2 Options selected")
    },
    focus:function(cmp){
        $A.util.addClass(cmp.find('resultsDiv'),'slds-is-open');
    },
    select:function(cmp){
        $A.util.addClass(cmp.find('resultsDiv2'),'slds-is-selected');
    }
       
           
    
});