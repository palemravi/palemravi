({
	doInit : function(component, event, helper) {
		var action = component.get("c.fetchOptions");
        action.setCallback(this,function(res) {
            alert(JSON.stringify(res.getReturnValue()));
            component.set("v.options",res.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    handleSelectChangeEvent : function(component, event, helper) {
    	alert('changed'+event.getParam("values"));
    }
})