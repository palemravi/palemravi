({
    handleInfo : function (component, event, helper) {
        var mssg = "info";
        helper.showToastMessage(mssg, event);
    },
    handleDelete : function (component, event, helper) {
        var mssg = "error";
        helper.showToastMessage(mssg, event);
    },
    handleConfirm : function (component, event, helper) {
        var mssg = "success";
        helper.showToastMessage(mssg, event);
    },
    handleWarning : function (component, event, helper) {
        var mssg = "warning";
        helper.showToastMessage(mssg, event);
    }    
});